package om.taotao.solrj;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.junit.Test;

public class SolrTest {
	@Test
	public void TestSolr() throws Exception {
		//创建链接
		SolrServer solrServer =new HttpSolrServer("http://192.168.1.8:8080/solr");
		//创建一个文档
		SolrInputDocument document=new SolrInputDocument();
		//添加域
		document.addField("id","solrTest_one");
		document.addField("item_title", "测试商品");
		document.addField("item_sell_point", "卖点");
		//添加到索引库
		solrServer.add(document);
		//提交
		solrServer.commit();
	}
	
	@Test
	public void TestQuery() throws Exception {
		//创建链接
		SolrServer solrServer =new HttpSolrServer("http://192.168.1.8:8080/solr");
		//创建链接对象
		SolrQuery query=new SolrQuery();
		query.setQuery("*:*");
		//执行查询
		QueryResponse response = solrServer.query(query);
		//取出查询结果
		SolrDocumentList lists = response.getResults();
		for (SolrDocument list : lists) {
			System.out.println(list.get("id"));
			System.out.println(list.get("item_title"));
			System.out.println(list.get("item_sell_point"));
		}
	}
	@Test
	public void testSolrCloud() throws Exception{
		//创建SolrServer对象
		CloudSolrServer slorServer = new CloudSolrServer("192.168.1.8:2181,192.168.1.8:2182,192.168.1.8:2183");
		//设置默认的coollection
		slorServer.setDefaultCollection("collection2");
		//创建一个文档对象
		SolrInputDocument document=new SolrInputDocument();
		document.addField("id", "test01");
		document.addField("item_title", "title");
		//添加文档
		slorServer.add(document);
		//提交
		slorServer.commit();
	}
}
