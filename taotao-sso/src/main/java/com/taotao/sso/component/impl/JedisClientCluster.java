package com.taotao.sso.component.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.taotao.sso.component.JedisClient;

import redis.clients.jedis.JedisCluster;

/**   
* @Title: JedisClientCluster.java 
* @Package com.taotao.rest.component.impl 
* @Description: redis集群版实现类
* @author 晓风过境   
* @date 2017年12月17日 下午11:10:01 
* @version V1.0   
*/
public class JedisClientCluster implements JedisClient{
	
	@Autowired	
	private JedisCluster jedisClient; 
	
	
	@Override
	public String set(String key, String value) {
		return jedisClient.set(key, value);
		
	}

	@Override
	public String get(String key) {
		return jedisClient.get(key);
	}

	@Override
	public Long hset(String key, String item, String value) {
		return jedisClient.hset(key, item, value);
	}

	@Override
	public String hget(String key, String item) {
		return jedisClient.hget(key, item);
	}

	@Override
	public Long incr(String key) {
		return jedisClient.incr(key);
	}

	@Override
	public Long decr(String key) {
		return jedisClient.decr(key);
	}

	@Override
	public Long expire(String key, int second) {
		return jedisClient.expire(key,second);
	}

	@Override
	public Long ttl(String key) {
		return jedisClient.ttl(key);
	}

	@Override
	public Long hdel(String key, String item) {
		return jedisClient.hdel(key, item);
	}

}
