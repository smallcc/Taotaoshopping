package com.taotao.sso.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.taotao.common.pojo.TaotaoResult;

public interface LoginService {
	//登录验证
	TaotaoResult login(String username,String password,HttpServletRequest request,HttpServletResponse response);
	//获取token
	TaotaoResult getUserByToken(String token);
	//安全退出
	TaotaoResult loginOut(String token);
}
