package com.taotao.sso.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**   
* @Title: PageController.java 
* @Package com.taotao.sso.controller 
* @Description: 注册和登录页面 
* @author 晓风过境   
* @date 2018年1月4日 下午8:47:49 
* @version V1.0   
*/
@Controller
public class PageController {
	//展示注册页面
	@RequestMapping("/page/login")
	public String showLogin(String redirectURL,Model model) {
		//传递参数给jspm
		model.addAttribute("redirect",redirectURL);
		return "login";
	}
	/**
	 * 展示注册页面
	 * */
	@RequestMapping("/page/register")
	public String showRegister() {
		return "register";
	}
}


