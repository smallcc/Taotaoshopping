package com.taotao.common.pojo;

import java.util.List;

/**
 * 共用工具类的使用
 * @author 晓风过境
 * @version 1.0
 * */
public class EasyUIDataGridResult {
	public EasyUIDataGridResult(long total, List<?> rows) {
		this.total = total;
		this.rows = rows;
	}
	public EasyUIDataGridResult() {
	}
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	public List<?> getRows() {
		return rows;
	}
	public void setRows(List<?> rows) {
		this.rows = rows;
	}
	
		private long total;
		private List<?> rows;
		//T 与 ? --T是已经确定了有泛型  ?表示什么类型都行没有也行
}
