package com.taotao.portal.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.pojo.TbItem;
import com.taotao.portal.pojo.CartItem;
import com.taotao.portal.service.CartService;
import com.taotao.portal.service.ItemService;
import com.taotao.utils.CookieUtils;
import com.taotao.utils.ExceptionUtil;
import com.taotao.utils.JsonUtils;

/**   
* @Title: CartServiceImpl.java 
* @Package com.taotao.portal.service.impl 
* @Description: 购物车实现Service
* @author 晓风过境   
* @date 2018年1月8日 下午2:58:47 
* @version V1.0   
*/
@Service
public class CartServiceImpl implements CartService {
	@Autowired
	private ItemService itemService;
	
	@Value("${COOKIE_EXPIRE}")
	private Integer COOKIE_EXPIRE;
	@Override
	public TaotaoResult addCart(Long itemId, Integer itemNum,HttpServletRequest request,HttpServletResponse response) {
//		1、接收商品id
//		2、从cookie中购物车商品列表
		List<CartItem> itemList = getCartItemList(request);
//		3、从商品列表中查询列表是否存在此商品
		boolean haveFlg=false;
		for (CartItem cartItem : itemList) {
//		4、如果存在商品的数量加上参数中的商品数量
			//如果商品存在数量相加
			if(cartItem.getId().longValue() == itemId.longValue()) {
				cartItem.setNum(cartItem.getNum()+itemNum);
				haveFlg=true;
				break;
			}
			//不存在
		}
//		5、如果不存在，调用rest服务，根据商品id获得商品数据。
		if(!haveFlg) {
			TbItem item = itemService.getItemById(itemId);
			//转换成购物车cartItem
			CartItem cartitem=new CartItem();
			cartitem.setId(itemId);
			cartitem.setNum(itemNum);
			cartitem.setPrice(item.getPrice());
			cartitem.setTitle(item.getTitle());
			if (StringUtils.isNotBlank(item.getImage())) {
				String image = item.getImage();
				String[] strings = image.split(",");
				cartitem.setImage(strings[0]);
			}

//		6、把商品数据添加到列表中
			itemList.add(cartitem);
		}
//		7、把购物车商品列表写入cookie
		CookieUtils.setCookie(request, response, "TT_CART", JsonUtils.objectToJson(itemList), COOKIE_EXPIRE, true);
//		8、返回TaotaoResult
		return TaotaoResult.ok();
	}
	
	/**
	 * 取商品的列表
	 * @param request
	 * @return
	 */
	private List<CartItem> getCartItemList(HttpServletRequest request){
		try {
			//从cookie中取数据
			String json = CookieUtils.getCookieValue(request, "TT_CART",true);
			//json转换java对象
			List<CartItem> list = JsonUtils.jsonToList(json, CartItem.class);
			return list==null?new ArrayList<CartItem>():list;
			
		} catch (Exception e) {
			return new ArrayList<CartItem>();
		}
		
	}

	@Override
	public List<CartItem> getItemCarts(HttpServletRequest request) {
		List<CartItem> list = getCartItemList(request);
		return list;
	}
	
	@Override
	public TaotaoResult updateCartItem(long itemId, Integer itemNum, HttpServletRequest request,
			HttpServletResponse response) {
		// 从cookie中取出来
		List<CartItem> itemList=getCartItemList(request);
		//根据商品的Id来查
		for (CartItem cartItem : itemList) {
			if(cartItem.getId()==itemId) {
				//更新数量
				cartItem.setNum(itemNum);
				break;
			}
		}
		//把购物车商品列表写入cookie
		CookieUtils.setCookie(request, response, "TT_CART", JsonUtils.objectToJson(itemList), COOKIE_EXPIRE, true);
		return TaotaoResult.ok();
	}

	@Override
	public TaotaoResult deleteCartItem(long itemId, HttpServletRequest request, HttpServletResponse response) {
//		1、接收商品id
//		2、从cookie中取购物车商品列表
		List<CartItem> itemList=getCartItemList(request);
//		3、找到对应id的商品
		for (CartItem cartItem : itemList) {
			if(cartItem.getId()==itemId) {
//		4、删除商品。
				itemList.remove(cartItem);
				break;
			}
		}
//		5、再重新把商品列表写入cookie。
		CookieUtils.setCookie(request, response, "TT_CART", JsonUtils.objectToJson(itemList), COOKIE_EXPIRE, true);
//		6、返回成功
		return TaotaoResult.ok();
	}
	
}
