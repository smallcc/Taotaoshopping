package com.taotao.portal.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.portal.pojo.CartItem;

public interface CartService {
	//添加购物车
	TaotaoResult addCart(Long itemId,Integer itemNum,HttpServletRequest request,HttpServletResponse response);
	//
	List<CartItem> getItemCarts(HttpServletRequest request);
	//更新购物车
	TaotaoResult updateCartItem(long itemId, Integer itemNum, HttpServletRequest request,HttpServletResponse response) ;
	//删除购物车的商品
	TaotaoResult deleteCartItem(long itemId, HttpServletRequest request,HttpServletResponse response);
}
