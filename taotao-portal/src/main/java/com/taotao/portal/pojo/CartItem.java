package com.taotao.portal.pojo;

import java.util.Date;

/**   
* @Title: CartItem.java 
* @Package com.taotao.portal.pojo 
* @Description: 购物车商品POJO
* @author 晓风过境   
* @date 2018年1月8日 下午3:04:36 
* @version V1.0   
*/
public class CartItem {
	 private Long id;

	    private String title;

	    private Long price;

	    private Integer num;

	    private String image;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public Long getPrice() {
			return price;
		}

		public void setPrice(Long price) {
			this.price = price;
		}

		public Integer getNum() {
			return num;
		}

		public void setNum(Integer num) {
			this.num = num;
		}

		public String getImage() {
			return image;
		}

		public void setImage(String image) {
			this.image = image;
		}

}
