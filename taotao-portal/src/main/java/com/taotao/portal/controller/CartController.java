package com.taotao.portal.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.portal.pojo.CartItem;
import com.taotao.portal.service.CartService;

/**   
* @Title: CartController.java 
* @Package com.taotao.portal.controller 
* @Description: 购物车Controller
* @author 晓风过境   
* @date 2018年1月8日 下午4:19:36 
* @version V1.0   
*/
@Controller
public class CartController {

	@Autowired
	private CartService cartService;

	/**
	 * 请求的URL：/cart/add/{itemId}.html?num=1 返回逻辑视图String 添加购物车
	 * 
	 * @param itemId
	 *            商品的ID
	 * @param num
	 *            商品的数量
	 * @return
	 */
	@RequestMapping("cart/add/{itemId}")
	public String addCartItem(@PathVariable Long itemId, Integer itemNum,
			HttpServletRequest request, HttpServletResponse response) {
		TaotaoResult result = cartService.addCart(itemId, itemNum, request,
				response);
		return "cartSuccess";
	}

	@RequestMapping("/cart/cart")
	public String getCartItemList(HttpServletRequest request, Model model) {
		List<CartItem> itemList = cartService.getItemCarts(request);
		model.addAttribute("cartList", itemList);
		return "cart";
	}
	
	//请求的url：/cart/update/num/{itemId}/{num}.action
	@RequestMapping("/cart/update/num/{itemId}/{num}")
	@ResponseBody
	public TaotaoResult updateCartItemNum(@PathVariable Long itemId,@PathVariable Integer num,HttpServletRequest request,HttpServletResponse response) {
		TaotaoResult result = cartService.updateCartItem(itemId, num, request, response);	
		return result;
	}
	
	@RequestMapping("/cart/delete/{itemId}")
	public String deleteCartItem(@PathVariable Long itemId,HttpServletRequest request,HttpServletResponse response) {
		TaotaoResult result = cartService.deleteCartItem(itemId, request, response);
		return "redirect:/cart/cart.html";
	}
}
