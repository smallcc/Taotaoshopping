package com.taotao.portal.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.taotao.portal.service.ContentService;


/**   
* @Title: IndexController.java 
* @Package com.taotao.portal.controller 
* @Description: 首页访问controller 
* @author 晓风过境   
* @date 2017年12月18日 下午3:12:44 
* @version V1.0   
*/
@Controller
public class IndexController {
	@Autowired
	private ContentService contentService;
	@RequestMapping(value="/index")
	public String ShowIndex(Model model) {
		//取大广告为的内容
		String json= contentService.getAd1List();
		//传递给页面
		model.addAttribute("ad1", json);
		return "index";
	}
	//postTest
	@RequestMapping(value="/posttest",method=RequestMethod.POST)
	@ResponseBody
	public String postTest(@RequestBody String name,@RequestBody String pass) {
		System.out.println(name+"\n"+pass);
		return "OK";
	}
}
