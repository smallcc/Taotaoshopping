package com.taotao.portal.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.taotao.pojo.TbUser;
import com.taotao.portal.pojo.CartItem;
import com.taotao.portal.pojo.OrderInfo;
import com.taotao.portal.service.CartService;
import com.taotao.portal.service.OrderService;

/**   
* @Title: OrderController.java 
* @Package com.taotao.portal.controller 
* @Description: 订单处理 
* @author 晓风过境   
* @date 2018年1月8日 下午10:32:28 
* @version V1.0   
*/
@Controller
@RequestMapping("/order")
public class OrderController {
	@Autowired
	private OrderService orderService;
	@Autowired
	private CartService cartService;
	@RequestMapping("/order-cart")
	public String showOderCart(Model model,HttpServletRequest request) {
		//去购物车商品列表
		List<CartItem> list = cartService.getItemCarts(request);
		model.addAttribute("cartList",list);
		return "order-cart";
	}
	//接收提交过来的订单信息
	//跳转订单成功页面
	@RequestMapping(value="/create",method=RequestMethod.POST)
	public String createOrder(OrderInfo orderInfo,Model model,HttpServletRequest request) {
		//取用户信息
		TbUser user=(TbUser) request.getAttribute("user");
		//补全orderInfo属性
		orderInfo.setUserId(user.getId());
		orderInfo.setBuyerNick(user.getUsername());
		//调用服务
		String orderId = orderService.createOrder(orderInfo);
		//把订单号传递给页面
		model.addAttribute("orderId", orderId);
		model.addAttribute("payment", orderInfo.getPayment());
		DateTime dateTime=new DateTime();
		dateTime = dateTime.plusDays(3);
		model.addAttribute("date", dateTime.toString("yyyy-MM-dd"));
		//返回逻辑视图
		return "success";
	}
	
}
