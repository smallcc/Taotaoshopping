package com.taotao.service;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.pojo.TbContent;

public interface ContentService {
	//增
	TaotaoResult insertContent(TbContent content);
	//改
	TaotaoResult updateContent(TbContent content);
	//删
	TaotaoResult deleteContent(Long[] ids);
}
