package com.taotao.service;
/**
 * 图片回显接口
 * @author 晓风过境
 * @version 1.0
 * */

import org.springframework.web.multipart.MultipartFile;

import com.taotao.common.pojo.PictureResult;

public interface PictureService {
	//图片回显
	PictureResult uploadPic(MultipartFile picFile);
}
