package com.taotao.service;

import java.util.List;

import com.taotao.common.pojo.EasyUITreeNode;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.pojo.TbContentCategory;

public interface ContentCatgoryService {
		//等到商品的分类管理信息
		List<EasyUITreeNode> getContentCatLost(Long parentId);
		//新增商品分类管理信息
		TaotaoResult insertCatgory(Long parenId,String name);
		//更新商品分类的管理信息
		TaotaoResult updateCatgory(Long id,String name);
		//删除节点
		TaotaoResult deletetCatgory(Long id);
		//查询父节点
		List<TbContentCategory> getChildNodeList(Long id);
}