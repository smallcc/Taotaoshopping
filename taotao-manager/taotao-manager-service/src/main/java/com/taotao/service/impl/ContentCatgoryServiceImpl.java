package com.taotao.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.taotao.common.pojo.EasyUITreeNode;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.mapper.TbContentCategoryMapper;
import com.taotao.pojo.TbContentCategory;
import com.taotao.pojo.TbContentCategoryExample;
import com.taotao.pojo.TbContentCategoryExample.Criteria;
import com.taotao.service.ContentCatgoryService;
/**
 * 内容分类管理
 * @author 晓风过境
 * @version 1.0
 * */
@Service
public class ContentCatgoryServiceImpl implements ContentCatgoryService {
	@Autowired
	private TbContentCategoryMapper contentCategoryMapper;
	@Override
	public List<EasyUITreeNode> getContentCatLost(Long parentId) {
		//根据parentId查询子节点
		TbContentCategoryExample example=new TbContentCategoryExample();
		Criteria criteria=example.createCriteria();
		criteria.andParentIdEqualTo(parentId);
		//执行查询
		List<TbContentCategory> list = contentCategoryMapper.selectByExample(example);
		List<EasyUITreeNode> resultlist=new ArrayList<EasyUITreeNode>();
		//转换成EasyUITreeNode列表
		for(TbContentCategory tbContentCategory : list) {
			//创建一个EasyUITreeNode节点
			EasyUITreeNode node=new EasyUITreeNode();
			node.setId(tbContentCategory.getId());
			node.setText(tbContentCategory.getName());
			node.setState(tbContentCategory.getIsParent()?"closed":"open");
			//加入resulist
			resultlist.add(node);
		}
		return resultlist;
	}
	@Override
	public TaotaoResult insertCatgory(Long parentId, String name) {
		//创建一个pojo对象
		TbContentCategory contentCategory=new TbContentCategory();
		contentCategory.setName(name);
		contentCategory.setParentId(parentId);
		//1（正常），2（删除）
		contentCategory.setStatus(1);
		contentCategory.setIsParent(false);
		//排列序号，表示同级目录的展现次序，如数值相等则安名称次序排列取汁返回大于：零的整数
		contentCategory.setSortOrder(1);
		contentCategory.setCreated(new Date());
		contentCategory.setUpdated(new Date());
		//插入数据
		contentCategoryMapper.insert(contentCategory);
		//取返回的主键
		Long id = contentCategory.getId();
		//判断父节点isparent属性
		//查询父节点
		TbContentCategory parentNode = contentCategoryMapper.selectByPrimaryKey(parentId);
		if(!parentNode.getIsParent()) {
			parentNode.setIsParent(true);
			//更新父节点
			contentCategoryMapper.updateByPrimaryKey(parentNode);
		}
		//返回主键
		return TaotaoResult.ok(id);
	}
	@Override
	public TaotaoResult updateCatgory(Long id, String name) {
		TbContentCategory category=contentCategoryMapper.selectByPrimaryKey(id);
		category.setName(name);
		contentCategoryMapper.updateByPrimaryKey(category);
		return TaotaoResult.ok();
	}
	/**
	 * 获取该节点下的子节点
	 * @return 父节点下的子节点
	 *@param id 父节点的Id 
	 **/
	@Override
	public TaotaoResult deletetCatgory(Long id) {
		deleteCatgoryAndChildNode(id);
		return TaotaoResult.ok();
	}
	
	private void deleteCatgoryAndChildNode(Long id) {
		//判断要删除的Category
		TbContentCategory tbContentCategory=contentCategoryMapper.selectByPrimaryKey(id);
		//判断是否是父节点
		if(tbContentCategory.getIsParent()) {
			//获取改父节点下的所有子节点
			List<TbContentCategory> list=getChildNodeList(id);
			//删除所有的子节点
			for(TbContentCategory category:list) {
				deleteCatgoryAndChildNode(category.getId());
			}
		}
			//判断父节点下是否还有其他的子节点
			if(getChildNodeList(tbContentCategory.getParentId()).size()==1) {
				//没有则将父节点标记为子节点
				TbContentCategory parentCategory=contentCategoryMapper.selectByPrimaryKey(tbContentCategory.getParentId());
				parentCategory.setIsParent(false);
				contentCategoryMapper.updateByPrimaryKey(parentCategory);
			}
			//删除节点
			contentCategoryMapper.deleteByPrimaryKey(id);
			return;
	}
	@Override
	public List<TbContentCategory> getChildNodeList(Long id) {
		//查询出所有父节点为传入Id的节点
		TbContentCategoryExample example=new TbContentCategoryExample();
		Criteria criteria=example.createCriteria();
		criteria.andParentIdEqualTo(id);
		return contentCategoryMapper.selectByExample(example);
	}

}
