package com.taotao.service;

import com.taotao.common.pojo.EasyUIDataGridResult;
import com.taotao.common.pojo.TaotaoResult;

public interface ItemParamService {
	//添加
	TaotaoResult insertItemParam(Long cid,String paramDate);
	//是否点击
	TaotaoResult checkItemParam(Long cid);
	//返回模版
	TaotaoResult getItemParemByCid(long cid);
	//删除模版参数
	TaotaoResult deleteItemParemById(Long[] ids);
	
}
