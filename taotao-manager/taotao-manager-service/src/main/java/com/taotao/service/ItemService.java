package com.taotao.service;

import com.taotao.common.pojo.EasyUIDataGridResult;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.pojo.TbItem;

public interface ItemService {
	//单个查询通过主键 查询某个记录
	TbItem getItemById(Long itemId);
	//分页结果
	EasyUIDataGridResult getItemList(int page,int rows);
	//给规格参数写出分页商品信息 //展示商品类目 founction——two
	EasyUIDataGridResult getItemParamList(int page,int rows);
	
	
	//提交商品信息
	TaotaoResult createItem(TbItem item,String desc,String itemParam);
	//根据商品Id查询规格参数
	String getItemParamHtml(Long itemId);
	
	//显示根据ID显示商品信息
	TbItem selectParamById(long id);
	
	
	//删除商品通过Id
	TaotaoResult deleteItemById(Long[] ids);
	//商品下架
	TaotaoResult updateItemInstock(Long[] ids);
	//商品上架
	TaotaoResult updateItemReshelf(Long[] ids);
}
