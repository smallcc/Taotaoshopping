package com.taotao.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.taotao.common.pojo.PictureResult;
import com.taotao.service.PictureService;
import com.taotao.utils.FastDfsClient;
/**
 * 图片上传Service实现
 * @author 晓风过境
 * @version 1.0
 **/
@Service
public class PictureServiceImpl implements PictureService {
	@Value("${IMAGE_SERVER_BASE_URL}")
	private String IMAGE_SERVER_BASE_URL;
	
	@Override
	public PictureResult uploadPic(MultipartFile picFile) {
		PictureResult	result=new PictureResult();
		//判断图片是否为空 
		if(picFile.isEmpty()) {
			//如果是空直接返回message
			result.setError(1);
			result.setMessage("图片为空");
			return result;
		}
		//上传到图片服务器
		try {
			FastDfsClient client=new FastDfsClient("classpath:properties/client.conf");
			//取出图片的扩展名
			String originalFileName=picFile.getOriginalFilename();
			String extName=originalFileName.substring(originalFileName.lastIndexOf(".")+1);
			String url=client.uploadFile(picFile.getBytes(), extName);
			url=IMAGE_SERVER_BASE_URL+url;
			//拼接图片地址
			result.setError(0);
			result.setUrl(url);
		} catch (Exception e) {
			result.setError(1);
			result.setMessage("图片上传失败");
			e.printStackTrace();
		}
		return result;
	}

}
