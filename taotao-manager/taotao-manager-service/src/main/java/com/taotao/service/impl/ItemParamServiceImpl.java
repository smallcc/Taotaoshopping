package com.taotao.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.taotao.common.pojo.EasyUIDataGridResult;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.mapper.TbItemParamExMapper;
import com.taotao.mapper.TbItemParamMapper;
import com.taotao.pojo.TbItemParam;
import com.taotao.pojo.TbItemParamExample;
import com.taotao.pojo.TbItemParamExample.Criteria;
import com.taotao.pojo.model.TbItemParamModel;
import com.taotao.service.ItemParamService;

/**
 * 商品规格参数模版的Service
 * @author 晓风过境
 * @version 1.0
 * */
@Service
public class ItemParamServiceImpl implements ItemParamService {
	@Autowired 
	private TbItemParamMapper itemParamMapper;
	@Autowired 
	private TbItemParamExMapper itemParamExMapper;
	@Override
	public TaotaoResult insertItemParam(Long cid, String paramData) {
		//创建一个pojo
		TbItemParam itemParam = new TbItemParam();
		itemParam.setItemCatId(cid);
		itemParam.setParamData(paramData);
		itemParam.setCreated(new Date());
		itemParam.setUpdated(new Date());
		//插入记录
		itemParamMapper.insert(itemParam);
		return TaotaoResult.ok();
	}

	@Override
	public TaotaoResult checkItemParam(Long cid) {
		//根据cid查询规格参数模板
		TbItemParamExample example = new TbItemParamExample();
		Criteria criteria = example.createCriteria();
		criteria.andItemCatIdEqualTo(cid);
		//执行查询
		List<TbItemParam>list = itemParamMapper.selectByExampleWithBLOBs(example);
		//判断是否查询到结果
		if (list != null&&list.size() > 0) {
			TbItemParam itemParam = list.get(0);
			return TaotaoResult.ok(itemParam);
		}
		return TaotaoResult.ok();
	}
	
	//返回模版
	@Override
	public TaotaoResult getItemParemByCid(long cid) {

		//创建查询条件
		TbItemParamExample example = new TbItemParamExample();
		Criteria criteria = example.createCriteria();
		criteria.andItemCatIdEqualTo(cid);
		List<TbItemParam> list = itemParamMapper.selectByExampleWithBLOBs(example);
		if (null != list && !list.isEmpty()) {
			return TaotaoResult.ok(list.get(0));
		}
		
		return TaotaoResult.build(400, "此分类未定义规格模板");
	}

	@Override
	public TaotaoResult deleteItemParemById(Long[] ids) {
		//开始变遍历删除
		for(Long id : ids) {
		itemParamMapper.deleteByPrimaryKey(id);
		}
		return TaotaoResult.ok();
	}
}
