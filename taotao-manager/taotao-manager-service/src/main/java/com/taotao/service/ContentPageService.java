package com.taotao.service;

import com.taotao.common.pojo.EasyUIDataGridResult;

public interface ContentPageService {
	EasyUIDataGridResult selectContentList(Long categoryId,int page ,int rows);
}
