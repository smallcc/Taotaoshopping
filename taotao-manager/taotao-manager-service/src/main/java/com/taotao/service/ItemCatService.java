package com.taotao.service;
import java.util.List;
import com.taotao.common.pojo.EasyUITreeNode;
import com.taotao.common.pojo.TreeNode;
/**
 * 根据id查询目录树
 * @author 晓风过境
 * @version
 * */
public interface ItemCatService {
	List<TreeNode> getItemCatList(long parentId);
}
