package com.taotao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.taotao.common.pojo.EasyUIDataGridResult;
import com.taotao.mapper.TbContentMapper;
import com.taotao.pojo.TbContent;
import com.taotao.pojo.TbContentExample;
import com.taotao.pojo.TbContentExample.Criteria;
import com.taotao.service.ContentPageService;

@Service
public class ContentPageServiceImpl implements ContentPageService{
	@Autowired
	private TbContentMapper contentMapper;
	@Override
	public EasyUIDataGridResult selectContentList(Long categoryId, int page, int rows) {
		//使用分页插件处理
		PageHelper.startPage(page,rows);
		//查询categoryId相匹配的所有TbContent
		TbContentExample example=new TbContentExample();
		Criteria criteria=example.createCriteria();
		criteria.andCategoryIdEqualTo(categoryId);
		List<TbContent> list = contentMapper.selectByExample(example);
		//获取共有多少页
		PageInfo<TbContent> pageInfo=new PageInfo<TbContent>(list);
		//创建结果集
		EasyUIDataGridResult result=new EasyUIDataGridResult();
		result.setRows(list);
		result.setTotal(pageInfo.getTotal());
		return result;
	}
	
}
