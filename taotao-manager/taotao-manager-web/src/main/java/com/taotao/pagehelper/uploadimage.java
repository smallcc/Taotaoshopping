package com.taotao.pagehelper;

import java.io.IOException;

import org.csource.common.MyException;
import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.StorageClient;
import org.csource.fastdfs.StorageServer;
import org.csource.fastdfs.TrackerClient;
import org.csource.fastdfs.TrackerServer;

public class uploadimage {
	public static void main(String[] args) throws Exception {
		upimg();
	}
	public static void upimg() throws IOException, MyException {
//		1、把FastDFS提供的jar包添加到工程中
//		2、初始化全局配置。加载一个配置文件。
		ClientGlobal.init("E:\\javaMaven\\taotao-manager\\taotao-manager-web\\src\\main\\resources\\properties\\client.conf");
//		3、创建一个TrackerClient对象。
		TrackerClient tc=new TrackerClient();
//		4、创建一个TrackerServer对象。
		TrackerServer ts=tc.getConnection();
//		5、声明一个StorageServer对象，null。
		StorageServer ss=null;
//		6、获得StorageClient对象。
		StorageClient sc=new StorageClient(ts,ss);
//		7、直接调用StorageClient对象方法上传文件即可。
		String[] str=sc.upload_file("C:\\Users\\Administrator\\Pictures\\n.jpg", "jpg", null);
		for(String stc:str) {
			System.out.println(stc);
		}
	}
}
