package com.taotao.controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * 页面展示controller
 * @author 晓风过境
 * @version 1.0
 * **/
@Controller
public class PageController {
	//显示主页面的
	//控制器就是返回一个包名 + 类名 + 方法名 
	//然后交给headler 去操作
	@RequestMapping(value="/")
	//这是一个headler
	public String showIndex() {
		return "index";
	}
	//打开网页
	@RequestMapping(value="/{page}")
	public String ShowPage(@PathVariable String page) {
		return page;
	}
	
}
