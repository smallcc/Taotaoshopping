package com.taotao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.taotao.common.pojo.PictureResult;
import com.taotao.service.PictureService;
import com.taotao.utils.JsonUtils;

/**
 * 图片按json的格式回显
 * @author 晓风过境
 * @version 1.0
 * */
@Controller
public class PictureController {
	
	@Autowired
	private PictureService pictureService;
	//注入URL地址
	@Value("${IMAGE_SERVER_BASE_URL}")
	private String IMAGE_SERVER_BASE_URL;
	@RequestMapping("/pic/upload")
	@ResponseBody
	public String uploadFile(MultipartFile uploadFile) {
		PictureResult result = pictureService.uploadPic(uploadFile);
		//打印出图片服务器的url
		//System.out.println("图片的URL地址:"+IMAGE_SERVER_BASE_URL);
		//把java对象手动转换成json string
		String json=JsonUtils.objectToJson(result);
		return json;
	}
}
