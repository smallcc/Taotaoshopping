package com.taotao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.taotao.common.pojo.EasyUIDataGridResult;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.pojo.TbItem;
import com.taotao.service.ItemService;

/**
 * 返回pojo数据 用json传
 * @author 晓风过境
 * @version 1.0
 * */

@Controller
public class ItemController {
	@Autowired
	private ItemService itemService;
	//测试查询一个分页对象
	@RequestMapping(value="/item/{itemId}")
	@ResponseBody
	private TbItem getItemById(@PathVariable long itemId) {
		TbItem item=itemService.getItemById(itemId);
		return item;
	}
	/**
	 * 查询商品列表
	 * <p>Title: getItemList</p>
	 * <p>Description: </p>
	 * @param page
	 * @param rows
	 * @return
	 */
	//将分页查询的信息返回个界面
	@RequestMapping(value="/item/list")
	@ResponseBody 
	public EasyUIDataGridResult getItemList(Integer page,Integer rows) {
		EasyUIDataGridResult result = itemService.getItemList(page, rows);
		return result;
	}
	//返回规格参数的分页
	@RequestMapping(value="/item/param/list")
	@ResponseBody 
	public EasyUIDataGridResult getItemParamList(Integer page,Integer rows) {
		EasyUIDataGridResult result = itemService.getItemParamList(page,rows);
		return result;
	} 
	//传值给数据库--新增商品
	@RequestMapping(value="/item/save",method=RequestMethod.POST)
	@ResponseBody
	public TaotaoResult createItem(TbItem item,String desc,String itemParams) {
		TaotaoResult result = itemService.createItem(item, desc,itemParams);
		return result;
	}
	
	//删除商品信息
	@RequestMapping("/rest/item/delete")
	@ResponseBody
	public TaotaoResult deleteItemById(Long[] ids) {
		return itemService.deleteItemById(ids);
	}
	//商品下架
	@RequestMapping("/rest/item/instock")
	@ResponseBody
	public TaotaoResult updateItemInstock(Long[] ids) {
		return itemService.updateItemInstock(ids);
	}
	//商品下架
	@RequestMapping("/rest/item/reshelf")
	@ResponseBody
	public TaotaoResult updateItemReshelf(Long[] ids) {
		return itemService.updateItemReshelf(ids);
	}
	//展示规格参数
	@RequestMapping(value="/page/item/{itemId}")
	public String showItemParam(@PathVariable Long itemId ,Model model) {
		String html = itemService.getItemParamHtml(itemId);
		model.addAttribute("myhtml",html);
		return "itemparam";
	}
	@RequestMapping(value="/rest/page/item-edit/{id}")
	@ResponseBody
	public TbItem selectById(@PathVariable Long id) {
		TbItem result = itemService.selectParamById(id);
		return result;
	}
	
}