package com.taotao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.taotao.common.pojo.EasyUIDataGridResult;
import com.taotao.common.pojo.EasyUITreeNode;
import com.taotao.service.ContentPageService;

/**
 * 内容列表分页展示
 * @author 晓风过境
 * @version 1.0
 * */
@Controller
public class ContentPageController {
	@Autowired
	private ContentPageService contentPageService;
	@RequestMapping("/content/query/list")
	@ResponseBody
	public EasyUIDataGridResult selectContentList(Long categoryId,int page,int rows) {
		return contentPageService.selectContentList(categoryId, page, rows);
		
	}
}
