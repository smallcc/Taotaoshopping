package com.taotao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.taotao.common.pojo.EasyUITreeNode;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.service.ContentCatgoryService;

/**
 * 内容分类管理
 * @author 晓风过境
 * @version 1.0
 * */
@Controller
@RequestMapping("/content/category")
public class ContentCatgoryController {
	@Autowired
	private ContentCatgoryService contentCatgoryService;
	@RequestMapping("/list")
	@ResponseBody
	public List<EasyUITreeNode> getContentCatList(@RequestParam(value="id",defaultValue="0") Long parentId){
		List<EasyUITreeNode> list = contentCatgoryService.getContentCatLost(parentId);
		return list;
	}
	@RequestMapping("/create")
	@ResponseBody
	public TaotaoResult createNode(Long parentId,String name) {
		TaotaoResult result = contentCatgoryService.insertCatgory(parentId, name);
		return result;
	}
	
	//更新
	@RequestMapping("/update")
	@ResponseBody
	public TaotaoResult updateNode(Long id,String name) {
		TaotaoResult result=contentCatgoryService.updateCatgory(id, name);
		return result;
	}
	//删除
	@RequestMapping("/delete")
	@ResponseBody
	public TaotaoResult deleteNode(Long id) {
		TaotaoResult result = contentCatgoryService.deletetCatgory(id);
		return result;
	}
	
}
