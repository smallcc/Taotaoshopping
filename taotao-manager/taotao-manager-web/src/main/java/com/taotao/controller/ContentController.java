package com.taotao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.pojo.TbContent;
import com.taotao.service.ContentService;
import com.taotao.utils.HttpClientUtil;


/**   
* @Title: ContentController.java 
* @Package com.taotao.controller 
* @Description: 内容管理
* @author 晓风过境   
* @date 2017年12月24日 下午7:16:26 
* @version V1.0   
*/
@Controller
public class ContentController {
	@Autowired
	private ContentService contentService;
	
	@Value("${REST_BASE_URL}")
	private String REST_BASE_URL;
	@Value("${REST_CONTENT_SYNO_URL}")
	private String REST_CONTENT_SYNO_URL;
	//新增
	@RequestMapping("/content/save")
	@ResponseBody
	public TaotaoResult insertContent(TbContent content) {
		TaotaoResult result=contentService.insertContent(content);
		//调用taotao-rest发布的服务，同步缓存
		HttpClientUtil.doGet(REST_BASE_URL+REST_CONTENT_SYNO_URL+content.getCategoryId());
		return result;
	}
	//修改
	@RequestMapping("/rest/content/edit")
	@ResponseBody
	public TaotaoResult updateContent(TbContent content) {
		return contentService.updateContent(content);
	}
	//删除
	@RequestMapping("/content/delete")
	@ResponseBody
	public TaotaoResult deleteContent(Long[] ids) {
		return contentService.deleteContent(ids);
	}
}
