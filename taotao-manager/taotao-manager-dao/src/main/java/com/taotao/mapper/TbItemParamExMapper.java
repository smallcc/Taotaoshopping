package com.taotao.mapper;

import java.util.List;

import com.taotao.pojo.model.TbItemParamModel;

public interface TbItemParamExMapper {
	List<TbItemParamModel> selectItemParamList(); 
}
