package com.taotao.jedis;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.taotao.rest.component.JedisClient;

public class testJedisClientSpring {
	@Test
	public void testJedisClientRingleSpring() throws Exception {
		//创建一个spring容器
		ApplicationContext applicationContext=new ClassPathXmlApplicationContext("classpath:spring/applicationContext-*.xml"); 
		//从容器中获取jedisClient对象
		JedisClient jedisClient = applicationContext.getBean(JedisClient.class);
		//jedisClient操作
		jedisClient.set("client1", "2000");
		String str=jedisClient.get("client1");
		System.out.println(str);
	}
}
