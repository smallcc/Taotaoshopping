package com.taotao.jedis;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

public class test {
	public static void main(String[] args) throws IOException {
		JedisPoolConfig poolConfig = new JedisPoolConfig();
		// 最大连接数
		poolConfig.setMaxTotal(1);
		// 最大空闲数
		poolConfig.setMaxIdle(1);
		// 最大允许等待时间，如果超过这个时间还未获取到连接，则会报JedisException异常：
		// Could not get a resource from the pool
		poolConfig.setMaxWaitMillis(1000);
		Set<HostAndPort> nodes = new LinkedHashSet<HostAndPort>();
		nodes.add(new HostAndPort("192.168.1.8", 7001));
		nodes.add(new HostAndPort("192.168.1.8", 7002));
		nodes.add(new HostAndPort("192.168.1.8", 7003));
		nodes.add(new HostAndPort("192.168.1.8", 7004));
		nodes.add(new HostAndPort("192.168.1.8", 7005));
		nodes.add(new HostAndPort("192.168.1.8", 7006));
		JedisCluster cluster = new JedisCluster(nodes, poolConfig);
		String name = cluster.get("test");
		System.out.println(name);
		cluster.set("age", "18");
		System.out.println(cluster.get("age"));
		cluster.close();
	}
}
