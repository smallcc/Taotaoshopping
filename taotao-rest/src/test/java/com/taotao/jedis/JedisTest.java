package com.taotao.jedis;

import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Test;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;

public class JedisTest {
	//单机版测试
		@Test
		public void testJedisSingle() throws Exception {
			//创建一个Jedis对象
			Jedis jedis = new Jedis("192.168.1.8", 6379);
			jedis.set("test", "hello smallchuan");
			String string = jedis.get("test");
			System.out.println(string);
			jedis.close();
		}
		//使用链接池
		@Test
		public void testJedisPool() throws Exception {
			//创建以后链接池对象
			//系统应该是单例的
			JedisPool jedisPool=new JedisPool("192.168.1.8",6379);
			//从链接池中获得一个链接
			Jedis jedis=jedisPool.getResource();
			String str = jedis.get("test");
			System.out.println(str);
			//jedis关闭
			jedis.close();
			//系统关闭时关闭链接池
			jedisPool.close();
		}
//		@Test
//		public void testJedisCluster() throws Exception {
//			//创建一个JedisCluster对象
//			Set<HostAndPort> nodes = new HashSet<>();
//			nodes.add(new HostAndPort("192.168.1.8", Integer.valueOf(7001)));
//			nodes.add(new HostAndPort("192.168.1.8", Integer.valueOf(7002)));
//			nodes.add(new HostAndPort("192.168.1.8", Integer.valueOf(7003)));
//			nodes.add(new HostAndPort("192.168.1.8", Integer.valueOf(7004)));
//			nodes.add(new HostAndPort("192.168.1.8", Integer.valueOf(7005)));
//			nodes.add(new HostAndPort("192.168.1.8", Integer.valueOf(7006)));
//			//在nodes中指定每个节点的地址
//			//jedisCluster在系统中是单例的。
//			JedisCluster jedisCluster = new JedisCluster(nodes);
//			jedisCluster.set("name", "zhangsan");
//			jedisCluster.set("value", "100");
//			String name = jedisCluster.get("name");
//			String value = jedisCluster.get("value");
//			System.out.println(name);
//			System.out.println(value);
//			
//			
//			//系统关闭时关闭jedisCluster
//			jedisCluster.close();
//		}
		@Test
		public void testJedisCluster() throws Exception {
			Set<HostAndPort> nodes = new LinkedHashSet<HostAndPort>();  
			nodes.add(new HostAndPort("192.168.1.8", 7001));
			nodes.add(new HostAndPort("192.168.1.8", 7002));
			nodes.add(new HostAndPort("192.168.1.8", 7003));
			nodes.add(new HostAndPort("192.168.1.8", 7004));
			nodes.add(new HostAndPort("192.168.1.8", 7005));
			nodes.add(new HostAndPort("192.168.1.8", 7006));
			//在nodes中指定每个节点的地址
			//jedisCluster在系统中是单例的。
			JedisCluster jedisCluster = new JedisCluster(nodes);
			jedisCluster.set("name", "zhangsan");
			jedisCluster.set("value", "100");
			String name = jedisCluster.get("name");
			String value = jedisCluster.get("value");
			System.out.println(name);
			System.out.println(value);
		}
		
}
