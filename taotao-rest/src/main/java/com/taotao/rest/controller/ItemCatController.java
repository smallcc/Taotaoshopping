package com.taotao.rest.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.taotao.rest.pojo.ItemCatResult;
import com.taotao.rest.service.ItemCatService;
import com.taotao.utils.JsonUtils;

/**
 * 商品分类查询服务 Controller
 * @author 晓风过境
 * @version 1.0
 * */
@Controller
@RequestMapping("/item/cat")
public class ItemCatController {
	@Autowired
	private ItemCatService itemCatService;
	//方法一
//	@RequestMapping(value="/list",produces=MediaType.APPLICATION_JSON_VALUE+";charset=utf-8")
//	@ResponseBody
//	public String getItemCatList(String callback) {
//		ItemCatResult result = itemCatService.getItemCatList();
//		if(StringUtils.isBlank(callback)) {
//			//需要吧result转换成字符串
//			String json=JsonUtils.objectToJson(result);
//			return json;
//		}
//		//如果不为空支持jsonp调用
//		String json=JsonUtils.objectToJson(result);
//		return callback+"("+json+");";
//	}
	//方法2
	@RequestMapping(value="/list")
	@ResponseBody
	public Object getItemCatList(String callback) {
		ItemCatResult result = itemCatService.getItemCatList();
		if(StringUtils.isBlank(callback)) {
			//需要吧result转换成字符串
			return result;
		}
		//如果不为空支持jsonp调用
		 MappingJacksonValue mappingJacksonValue = new MappingJacksonValue(result);
		 mappingJacksonValue.setJsonpFunction(callback);
		 return mappingJacksonValue;
	}
}
