package com.taotao.rest.service;

import com.taotao.pojo.TbItem;
import com.taotao.pojo.TbItemDesc;
import com.taotao.pojo.TbItemParamItem;

public interface ItemService {
	//商品信息参数
	TbItem getItemById(Long itemId);
	//商品信息描述
	TbItemDesc getItemDescById(Long itemId);
	//商品规格参数
	TbItemParamItem getItemParamById(Long itemId);
}
