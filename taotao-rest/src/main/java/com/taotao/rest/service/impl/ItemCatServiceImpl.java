package com.taotao.rest.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.StringUtils;
import com.taotao.mapper.TbItemCatMapper;
import com.taotao.pojo.TbItemCat;
import com.taotao.pojo.TbItemCatExample;
import com.taotao.pojo.TbItemCatExample.Criteria;
import com.taotao.rest.component.JedisClient;
import com.taotao.rest.pojo.ItemCatNode;
import com.taotao.rest.pojo.ItemCatResult;
import com.taotao.rest.service.ItemCatService;
import com.taotao.utils.JsonUtils;

/**
 * 商品分类列表查询
 * <p>Title: ItemCatServiceImpl</p>
 * <p>Description: </p>
 * <p>Company: www.itcast.com</p> 
 * @author 晓风过境
 * @version 1.0
 */
@Service
public class ItemCatServiceImpl implements ItemCatService {

	@Autowired
	private TbItemCatMapper itemCatMapper;
	
   @Value("${INDEX_ITEMCAT_REDIS_KEY}")
   private String INDEX_ITEMCAT_REDIS_KEY;
	  
	@Autowired
	private JedisClient jedisClient;
	
	@Override
	public ItemCatResult getItemCatList() {
		// -----------------------------缓存读取---------------------------------------
        try {
            String result = jedisClient.get(INDEX_ITEMCAT_REDIS_KEY);
            if (!StringUtils.isBlank(result)) {
                List<ItemCatNode> resultlist = JsonUtils.jsonToList(result, ItemCatNode.class);
                ItemCatResult catResult = new ItemCatResult();
                // 查询分类列表
                catResult.setData(resultlist);
                return catResult;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //---------------------------------------------缓存添加-------------------- 
		ItemCatResult result = new ItemCatResult();
		result.setData(getList(0));
		return result;
	}
	
	/**
	 * 递归方法，根据parent查询一个树形列表
	 * <p>Title: getList</p>
	 * <p>Description: </p>
	 * @param parentId
	 * @return
	 */
	private List<?> getList(long parentId) {
		//创建查询条件
		TbItemCatExample example = new TbItemCatExample();
		Criteria criteria = example.createCriteria();
		criteria.andParentIdEqualTo(parentId);
		List<TbItemCat> list = itemCatMapper.selectByExample(example);
		List resultList = new ArrayList<>();
		//循环计数
		int count = 0;
		for (TbItemCat tbItemCat : list) {
			//如果为父节点
			if (tbItemCat.getIsParent()) {
				ItemCatNode node = new ItemCatNode();
				node.setUrl("/products/" + tbItemCat.getId() + ".html");
				//判断是否为第一层节点
				if (parentId == 0) {
					node.setName("<a href='"+node.getUrl()+"'>"+tbItemCat.getName()+"</a>");
				} else {
					node.setName(tbItemCat.getName());
				}
				node.setItems(getList(tbItemCat.getId()));
				resultList.add(node);
			} else {
				String node = "/products/"+tbItemCat.getId()+".html|" + tbItemCat.getName();
				resultList.add(node);
			}
			count++;
			//第一个层循环，只取14条记录
			if (parentId == 0 && count >= 14) {
				break;
			}
		}
		 //---------------------------------------------缓存存储--------------------     
        //从缓存中添加内容
        try{
            //把list转换成字符串
            String cacheString=JsonUtils.objectToJson(resultList);
            jedisClient.set(INDEX_ITEMCAT_REDIS_KEY, cacheString);

        }catch(Exception e){
            e.printStackTrace();
        }
        //----------------------------------------------------------------
		return resultList;
	}

}
